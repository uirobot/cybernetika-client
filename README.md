# Cybernetika Client

Клиент для образовательной платформы Кибернетика.

## Версия 0.1.4
### [WINDOWS (zip архив)](https://gitlab.com/uirobot/cybernetika-client/raw/master/WINDOWS/windows.zip)
### [MACOSX (zip архив)](https://gitlab.com/uirobot/cybernetika-client/raw/master/MACOSX/macos.zip)

----

#### Что такое Cybernetica Client?
Cybernetica Client это образовательная платформа, для обучения Scratch (http://cybernetika.pro/). С помощью платформы ученик сможет получать и отправлять на проверку задания. 


#### Как запустить?
Вам необходимо скачать версию для вашей операционной системы, распокавать файл и запустить файлы cyberneticaXX.X.X.exe (для Windows) или cybernetica (для MacOSX). После запуска, откроется ваш текущий браузер с адресом: http://localhost:3001.


#### Как зарегистрироваться?
Что бы получать логин и пароль, зарегистрируйтесь на сайте http://cybernetika.pro/